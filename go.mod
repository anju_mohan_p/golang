module api-test

go 1.17

require github.com/gorilla/mux v1.8.0

require github.com/thedevsaddam/gojsonq v2.3.0+incompatible

// require (
// 	github.com/AlekSi/gocov-xml v1.0.0 // indirect
// 	github.com/jstemmer/go-junit-report v0.9.1 // indirect
// 	github.com/stretchr/objx v0.1.0 // indirect
// 	github.com/yuin/goldmark v1.4.0 // indirect
// 	golang.org/x/mod v0.4.2 // indirect
// 	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
// 	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
// 	golang.org/x/tools v0.1.7 // indirect
// 	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
// )

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
