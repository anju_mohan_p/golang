package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

var payloads []Payload

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/vehicle/", vehLookUp).Methods("POST")
	return router
}
func TestVehicleLookUp(t *testing.T) {
	payloads = append(payloads, Payload{
		RegistrationNumber: "",
		PhoneNumber:        "",
		EmailAddress:       "",
		OrganisationName:   "Org One",
		FirstName:          "",
		LastName:           ""})
	payloads = append(payloads, Payload{
		RegistrationNumber: "",
		PhoneNumber:        "",
		EmailAddress:       "",
		OrganisationName:   "",
		FirstName:          "",
		LastName:           ""})

	payloads = append(payloads, Payload{
		RegistrationNumber: "zz",
		PhoneNumber:        "",
		EmailAddress:       "",
		OrganisationName:   "",
		FirstName:          "",
		LastName:           ""})
	payloads = append(payloads, Payload{})
	for _, payLd := range payloads {

		testResposneCode(t, payLd)

	}

}

func testResposneCode(t *testing.T, payLd Payload) {

	jsonPayload, _ := json.Marshal(payLd)
	request, _ := http.NewRequest("POST", "/vehicle/", bytes.NewBuffer(jsonPayload))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	fmt.Println("response.Code", response.Code)
	assert.Equal(t, 200, response.Code, response.Body.String())

}
