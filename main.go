package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/gojsonq"
)

func main() {
	router := mux.NewRouter()
	//	router.HandleFunc("/vehicle/", getAllVehicles).Methods("GET")
	router.HandleFunc("/vehicle/", vehLookUp).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}

// func getAllVehicles(writer http.ResponseWriter, req *http.Request) {
// 	writer.Header().Set("Content-Type", "application/json")
// 	jq := gojsonq.New().File("./vehicleData.json")
// 	res := jq.From("Vehicle.details").Get()
// 	fmt.Println(res)
// 	json.NewEncoder(writer).Encode(res)
// }

func vehLookUp(writer http.ResponseWriter, req *http.Request) {
	var payload Payload
	fmt.Println("Multiple param")
	errs := url.Values{}
	if err := json.NewDecoder(req.Body).Decode(&payload); err != nil {
		fmt.Println("err in decode")
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return

	} else if (Payload{} == payload) {
		fmt.Println("check payload")
		fmt.Println("Payload empty")
		writer.WriteHeader(http.StatusBadRequest)
		errs.Add("Message", "There should be atleast one search condition")
		json.NewEncoder(writer).Encode(errs)
	} else {
		fmt.Println("read from file")
		jq := gojsonq.New().File("./vehicleData.json")
		res := jq.From("Vehicle.details").WhereContains("Customer.individualOrOrganisation.organisation.organisationName", payload.OrganisationName).
			WhereContains("registrationNumber", payload.RegistrationNumber).
			WhereContains("Customer.phoneNumber", payload.PhoneNumber).
			WhereContains("Customer.emailAddress", payload.EmailAddress).
			WhereContains("Customer.individualOrOrganisation.individual.lastName", payload.LastName).
			WhereContains("Customer.individualOrOrganisation.individual.firstName", payload.FirstName).Get()
		//	fmt.Println(reflect.TypeOf(res))
		str := fmt.Sprintf("%v", res)
		fmt.Println(str)
		if str == "[]" {
			writer.WriteHeader(http.StatusBadRequest)
			errs.Add("Message", "No Data Found")
			json.NewEncoder(writer).Encode(errs)
			return
		}

		json.NewEncoder(writer).Encode(res)
	}

}

type Payload struct {
	RegistrationNumber string `json:"registrationNumber"`
	PhoneNumber        string `json:"phoneNumber"`
	EmailAddress       string `json:"emailAddress"`
	OrganisationName   string `json:"organisationName"`
	FirstName          string `json:"firstName"`
	LastName           string `json:"lastName"`
}
